﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class Electric : IVehicle
    {
        //Implementing IVehicle properties
        public Direction Direction { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Passengers { get; set; }
        public double EmissionIdle { get; set; }
        public double EmissionMoving { get; set; }
        //New property
        public IVehicle Vehicle { get; set; }

        /// <summary>
        /// It sets the vehicle property and subscribes for its events.
        /// </summary>
        /// <param name="vehicle"></param>
        public Electric(IVehicle vehicle)
        {
            this.Passengers = vehicle.Passengers;
            this.EmissionMoving = vehicle.EmissionMoving / 4;
            this.EmissionIdle = vehicle.EmissionIdle = 0;
            this.Vehicle = vehicle;
            this.Vehicle.Done += Vehicle_Done;
            this.Vehicle.Moved += Vehicle_Moved;
            this.Vehicle.Waiting += Vehicle_Waiting;
        }

        //Events
        public event VehicleActionEventHandler Done;
        public event VehicleActionEventHandler Moved;
        public event VehicleActionEventHandler Waiting;

        /// <summary>
        /// It checkes the vehicle's coordinate in grid to find if it's already in intersection then it returns true, 
        /// otherwise it returns false.
        /// </summary>
        /// <returns>boolean</returns>
        public virtual bool InIntersection()
        {
            if (this.Vehicle.InIntersection())
                return true;
            return false;
        }
        /// <summary>
        /// It invokes the vehicle Move() method by ISignalStrategy object that it has recieved as argument.
        /// </summary>
        /// <param name="signal"></param>

        public virtual void Move(ISignalStrategy signal)
        {
            this.Vehicle.Move(signal);
            
        }

        /// <summary>
        /// This checks the next tile in the vehicle's direction to find if it's intersection or not.
        /// </summary>
        /// <returns> boolean</returns>
        public virtual bool NextIsIntersection()
        {
            if (this.Vehicle.NextIsIntersection())
                return true;
            return false;
        }//End NextIsIntersection()


        /// <summary>
        /// Event handlers for the events of decorated Vehicle object. These handlers invoke the Electric object events.
        /// </summary>
        private void Vehicle_Waiting(IVehicle vehicle) => Waiting?.Invoke(this);

        private void Vehicle_Moved(IVehicle vehicle) => Moved?.Invoke(this);

        private void Vehicle_Done(IVehicle vehicle) => Done?.Invoke(this);

        
    }//End Electric
}
