﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public enum Direction
    {
        UP, DOWN, LEFT, RIGHT, NONE
    }

    public enum Colour
    {
        RED, GREEN, AMBER
    }
}
