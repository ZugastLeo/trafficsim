﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    /// <summary>
    /// ISignalStrategy Fixed Signal
    /// Follows constant time, disregards amount of Traffic
    /// </summary>
    public class FixedSignal : ISignalStrategy
    {
        private int[] timing;
        private bool isRightLeftOn = true;
        private Colour updown;
        private Colour rightleft;
        private int counter = 0;

        /// <summary>
        /// Constructor for FixedSignal
        /// </summary>
        /// <param name="timing">int[] representing {leftRight time in green, leftright time in amber, updown time in green, updown time in amber}</param>
        public FixedSignal(params int[] timing)
        {
            this.timing = timing;
            rightleft = Colour.GREEN;
            updown = Colour.RED;
        }

        /// <summary>
        /// returns the colour of the given direction
        /// </summary>
        /// <param name="dir">Direction of desired Colour</param>
        /// <returns></returns>
        public Colour GetColour(Direction dir)
        {
            Colour toReturn = Colour.RED;
            switch (dir)
            {
                case Direction.DOWN:
                case Direction.UP:
                    toReturn = updown;
                    break;
                case Direction.LEFT:
                case Direction.RIGHT:
                    toReturn = rightleft;
                    break;
            }
            return toReturn;
        }
        
        //"1" = rightleft; "2" = updown
        //green1 time = timing[0];
        //amber1 time = timing[1];
        //red1 time = timing[2] + timing[3];
        //green2 time = timing[2];
        //amber2 time = timing[3];
        //red2 time = timing[0] + timing[1];

        //time during rotation
        //starts green
        //time during green ends change to amber
        //time during amber ends change to red
        //time during red ends change back to green and reset time

        /// <summary>
        /// Sets the light colours depending on time
        /// </summary>
        public void Update()
        {
            counter++;
            if (isRightLeftOn ? counter == timing[0] : counter == timing[2])
            {
                if (isRightLeftOn)
                {
                    rightleft = Colour.AMBER;
                }
                else
                {
                    updown = Colour.AMBER;
                }
            }
            else if (isRightLeftOn ? counter == timing[0] + timing[1] : counter == timing[2] + timing[3])
            {
                if (isRightLeftOn)
                {
                    updown = Colour.GREEN;
                    rightleft = Colour.RED;
                }
                else
                {
                    rightleft = Colour.GREEN;
                    updown = Colour.RED;
                }
                isRightLeftOn = !isRightLeftOn;
                counter = 0;
            }
        }
    }
}
