﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class Grass : Tile
    {
        /// <summary>
        /// Creates a Grass object and sets its Direction
        /// </summary>
        public Grass() : base(Direction.NONE)
        {

        }
    }
}
