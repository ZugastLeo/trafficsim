﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class Grid
    {
        private Tile[,] grid;
        public int Size { get { return grid.GetLength(0); } }

        /// <summary>
        /// Creates a Grid object that contains the given tile array
        /// </summary>
        /// <param name="g"></param>
        public Grid(Tile[,] g)
        {
            //test for a square grid
            if (g.GetLength(0) != g.GetLength(1)) {
                throw new ArgumentException("The Grid Must be a Square! The current dimensions are: (Width:" + grid.GetLength(0) + ", Height:" + grid.GetLength(1) + ")");
            }
            this.grid = g;
        }

        /// <summary>
        /// The grid indexer returns the Tile in the given coordinates
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        public Tile this[int x, int y]
        {
            get
            {
                return InBounds(x, y) ? grid[x, y] : throw new IndexOutOfRangeException("The coordinates given to the Grid are out of bounds!");
            }
        }

        /// <summary>
        /// Returns true if there is a vehicle in the given coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public bool IsOccupied(int x, int y)
        {
            return grid[x, y].Occupied;
        }

        /// <summary>
        /// Returns true if the given coordinate is inside the bounds of the grid
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public bool InBounds(int x, int y)
        {
            if (x < 0 || y < 0 || x > Size-1 || y > Size-1)
            {
                return false;
            }
            return true;
        }
    }
}
