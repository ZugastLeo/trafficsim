﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    /// <summary>
    /// ISignalStrategy interface
    /// represents the strategy used for the lights of an Intersection
    /// </summary>
    public interface ISignalStrategy
    {
        /// <summary>
        /// Update colours of intersection signals
        /// </summary>
        void Update();

        /// <summary>
        /// returns colour for given direction
        /// </summary>
        /// <param name="dir">Direction for desired Colour</param>
        /// <returns>the colour of the given Direction</returns>
        Colour GetColour(Direction dir);
    }
}
