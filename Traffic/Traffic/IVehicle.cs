﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    //Delecate declaration
    public delegate void VehicleActionEventHandler(IVehicle vehicle);

    public interface IVehicle
    {
        event VehicleActionEventHandler Done;
        event VehicleActionEventHandler Moved;
        event VehicleActionEventHandler Waiting;
        //Properties
        Direction Direction { get; set; }
        int X { get; set; }
        int Y { get; set; }
        int Passengers { get; set; }
        double EmissionIdle { get; set; }
        double EmissionMoving { get; set; }

        /// <summary>
        /// Verifies the light and the vehicle position in grid. If the vehicle is in bounds it invokes the Done event.
        /// If light is green or if the vehicle is already in intersection or if the next tile in vehicle's direction 
        /// is not the intersection and it's free it will move the vehicle and fires the move event.
        /// Otherwise if the light is amber or red and the next tile in vehicle's direction is intersection it will stop and 
        /// raises the waiting event.
        /// </summary>
        /// <param name="signal"></param>
        void Move(ISignalStrategy signal);

        /// <summary>
        /// It checkes the vehicle's coordinate in grid to find if it's already in intersection then it returns true, 
        /// otherwise it returns false.
        /// </summary>
        /// <returns></returns>
        bool NextIsIntersection();

        /// <summary>
        /// This checks the next tile in the vehicle's direction to find if it's intersection or not.
        /// </summary>
        /// <returns></returns>
        bool InIntersection();
    }
}
