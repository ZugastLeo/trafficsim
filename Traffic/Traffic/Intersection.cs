﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Traffic
{
    /// <summary>
    /// Intersection class
    /// </summary>
    public class Intersection : IEnumerable
    {
        private List<IVehicle> vehicles;
        private List<Vector2> startCoords;
        private Grid grid;
        private ISignalStrategy signal;
        private static Random random;
        private int carMoveCounter;

        /// <summary>
        /// Implementation for IEnumerable
        /// </summary>
        /// <returns>returns the Enumerator of the internal List of vehicles</returns>
        public IEnumerator GetEnumerator()
        {
            return vehicles.GetEnumerator();
        }

        /// <summary>
        /// Static constructor to initialize the random object
        /// </summary>
        static Intersection()
        {
            random = new Random();
        }

        /// <summary>
        /// Constructor for Intersection
        /// </summary>
        /// <param name="signal">Signal strategy to be used by the intersection</param>
        /// <param name="startCoords">Vector2 List representing every valid coordinate for a IVehicle to start</param>
        /// <param name="grid">Grid representing the Intersection</param>
        public Intersection(ISignalStrategy signal, List<Vector2> startCoords, Grid grid)
        {
            this.vehicles = new List<IVehicle>();
            this.grid = grid;
            this.signal = signal;
            this.startCoords = startCoords;
        }

        /// <summary>
        /// runs the Update method of every IVehicle in the intersection and the signal
        /// </summary>
        public void Update()
        {
            for (carMoveCounter = 0; carMoveCounter < this.vehicles.Count; carMoveCounter++)
            {
                vehicles[carMoveCounter].Move(signal);
            }
            signal.Update();
        }

        /// <summary>
        /// Adds a IVehicle to the intersection and places it randomly on grid at the begining of lanes
        /// </summary>
        /// <param name="v">IVehicle object to be added</param>
        public void Add(IVehicle v)
        {
            vehicles.Add(v);
            int startCoordIndex;
            do 
            {
                startCoordIndex = random.Next(0, startCoords.Count);

            } while (grid.IsOccupied((int)startCoords[startCoordIndex].X, (int)startCoords[startCoordIndex].Y));

            v.X = (int)startCoords[startCoordIndex].X;
            v.Y = (int)startCoords[startCoordIndex].Y;

            switch(grid[v.X, v.Y].Direction)
            {
                case Direction.DOWN:
                    v.Direction = Direction.DOWN;
                    break;
                case Direction.UP:
                    v.Direction = Direction.UP;
                    break;
                case Direction.RIGHT:
                    v.Direction = Direction.RIGHT;
                    break;
                case Direction.LEFT:
                    v.Direction = Direction.LEFT;
                    break;
                default:
                    throw new Exception("Direction is not DOWN, UP, RIGHT or LEFT which is not supposed to happen for a road tile");
            }
            v.Done += removeFromIntersection;
        }

        /// <summary>
        /// removes IVehicle from intersection
        /// </summary>
        /// <param name="v">IVehicle to be removed</param>
        private void removeFromIntersection(IVehicle v)
        {
            vehicles.Remove(v);
            carMoveCounter--;
        }
    }
}
