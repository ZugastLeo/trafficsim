﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class IntersectionTile : Tile
    {
        /// <summary>
        /// Creates an IntersectionTile object and sets Direction
        /// </summary>
        public IntersectionTile(): base(Direction.NONE)
        {

        }
    }
}
