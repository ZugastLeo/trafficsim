﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class Light : Tile
    {
        private ISignalStrategy strategy;
        public Colour Colour
        {
            get
            { 
                return strategy.GetColour(this.Direction);
            }
        }

        /// <summary>
        /// Creates a Light object and sets its Colour and Direction
        /// </summary>
        /// <param name="member"></param>
        /// <param name="d"></param>
        public Light(ISignalStrategy member, Direction d): base(d)
        {
            this.strategy = member;
        }
    }
}
