﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class Road : Tile
    {
        /// <summary>
        /// Creates a Road object and sets its Direction
        /// </summary>
        /// <param name="d"></param>
        public Road(Direction d): base(d)
        {
            
        }
    }
}
