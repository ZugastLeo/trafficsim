﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public abstract class Tile
    {
        //Properties for all classes derived from Tiles
        public Direction Direction { get; set; }
        public bool Occupied { get; set; }

        /// <summary>
        /// Constructor used to set the Direction of its derived classes
        /// </summary>
        /// <param name="d"></param>
        public Tile(Direction d)
        {
            this.Occupied = false;
            this.Direction = d;
        }

    }
}
