﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class Total
    {
        public int Passengers { get; set; }
        public double Emission { get; set; }
        
        /// <summary>
        /// Event handler for when the Vehicle is done with its' movement.
        /// It will be responsible of incrementing the amount of 
        /// passengers based on the vehicle.
        /// </summary>
        /// <param name="vehicle"></param>
        public void VehicleOver(IVehicle vehicle)
        {
            Passengers = Passengers + vehicle.Passengers;
        }

        /// <summary>
        /// Event handler for when the Vehicle is moving on he screen.
        /// It will be responsible of incrementing the emission produced by 
        /// each moving vehicle.
        /// </summary>
        /// <param name="vehicle"></param>
        public void Moved(IVehicle vehicle)
        {
            Emission = Emission + vehicle.EmissionMoving;
        }

        /// <summary>
        /// Event handler for when the Vehicle is idle on he screen.
        /// It will be responsible of incrementing the emission produced by 
        /// each moving vehicle.
        /// </summary>
        /// <param name="vehicle"></param>
        public void Waiting(IVehicle vehicle)
        {
            Emission = Emission + vehicle.EmissionIdle;
        }
    }
}
