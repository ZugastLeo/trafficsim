﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public class TrafficControl
    {
        private static Random random; 
        private int numVehicles = 0;
        private int percentCars = 0;
        private int percentElectric = 0;
        private int numOfGasCars = 0;
        private int numOfElecCars = 0;
        private int numOfGasCycle = 0;
        private int numOfElecCycle = 0;
        private int frequency = 0;
        private int callCount = 0;

        public Grid Grid{get; private set;}
        public Intersection Intersection{get; private set;}
        public Total Total{get; private set;}

        static TrafficControl()
        {
            random = new Random();
        }
        
        /// <summary>
        /// The method will be responsible of initializing the whole simulation.
        /// It is going to find the total amount of cars and motorcycles, the signal strategy and the starting coordinates.
        /// It will also instantiate the Grid, the Total and the Intersection objects.
        /// </summary>
        /// <param name="fileContent"></param>
        public void Parse(String fileContent)
        {
            //Store each line seperately 
            String[] configurations = fileContent.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            //Find totalCars, percentsCars, percentsElectric and frequency
            this.numVehicles = Int32.Parse(configurations[0].Trim());
            this.frequency = Int32.Parse(configurations[1].Trim());
            this.percentCars = Int32.Parse(configurations[2].Trim());
            this.percentElectric = Int32.Parse(configurations[3].Trim());
            int totalCars = (int)(this.numVehicles * (this.percentCars / 100.0));
            int totalCycles = this.numVehicles - totalCars;
            this.numOfElecCars = (int)(totalCars * (this.percentElectric / 100.0));
            this.numOfElecCycle = (int)(totalCycles * (this.percentElectric / 100.0));
            this.numOfGasCars = totalCars - this.numOfElecCars;
            this.numOfGasCycle = totalCycles - this.numOfElecCycle;
            

            //Instantiate FixedSignal(Strategy)
            String[] timesStringFormat = configurations[4].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Int32[] timesIntFormat = new Int32[timesStringFormat.Length];
            for (int i = 0; i < timesStringFormat.Length; i++)
            {
                timesIntFormat[i] = Int32.Parse(timesStringFormat[i]);
            }
            FixedSignal fixSigStrat = new FixedSignal(timesIntFormat);

            //Get the dimension of the grid
            int dimension = configurations[5].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length;
            
            //Setup  String layout into a 2D layout 
            String[][] gridString= new String[dimension][];
            for (int i = 5; i < configurations.Length; i++)
            {
                gridString[i - 5] = configurations[i].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            }


            //Create a Tile[,] that represents the layout
            Tile[,] layoutGrid = createTrafficBoardLayout(gridString,dimension,fixSigStrat);

            //Instantiate Grid
            this.Grid = new Grid(layoutGrid);

            //List of 4 starting coords
            List<Vector2> startCoords = CoordGenerator(gridString);

            //Instantiate Intersection
            this.Intersection = new Intersection(fixSigStrat, startCoords, this.Grid);

            //Instantiate Totals
            this.Total = new Total();
        }

        public void Update()
        {
            //Increment callCount each time the method is called
            this.callCount++;
            if(this.callCount == Int32.MaxValue)
            {
               this.callCount = 0;
            }
            //Only create vehicle at the given frequency
            if ((this.callCount % this.frequency) == 0)
            {
                createVehicle();
            }
        }

        /// <summary>
        /// Based on the grid, this method will be in charge of creating a Tile[,] layout with the correct tile type.
        /// </summary>
        /// <param name="gridString"></param>
        /// <param name="dimension"></param>
        /// <returns></returns>
        private Tile[,] createTrafficBoardLayout(String[][] gridString, int dimension, FixedSignal fixSigStrat)
        {
            //Build grid
            Tile[,] layoutGrid = new Tile[dimension, dimension];
            for (int i = 0; i < gridString.Length; i++)
            {
                for (int j = 0; j < gridString.Length; j++)
                {
                    String tileType = gridString[i][j];
                    switch (tileType)
                    {
                        case "U":
                            layoutGrid[j, i] = new Road(Direction.UP);
                            break;
                        case "D":
                            layoutGrid[j, i] = new Road(Direction.DOWN);
                            break;
                        case "R":
                            layoutGrid[j, i] = new Road(Direction.RIGHT);
                            break;
                        case "L":
                            layoutGrid[j, i] = new Road(Direction.LEFT);
                            break;
                        case "G":
                            layoutGrid[j, i] = new Grass();
                            break;
                        case "I":
                            layoutGrid[j, i] = new IntersectionTile();
                            break;
                        case "1":
                            layoutGrid[j, i] = new Light(fixSigStrat, Direction.DOWN);
                            break;
                        case "2":
                            layoutGrid[j, i] = new Light(fixSigStrat, Direction.LEFT);
                            break;
                        case "3":
                            layoutGrid[j, i] = new Light(fixSigStrat, Direction.RIGHT );
                            break;
                        case "4":
                            layoutGrid[j, i] = new Light(fixSigStrat, Direction.UP);
                            break;
                        default:
                            throw new Exception("There is an unexpected value in the grid configuration");
                    }
                }
            }
            return layoutGrid;
        }

 
        /// <summary>
        /// This method will return a list of coordinates based on the roads given in the layout.
        /// </summary>
        /// <param name="layout"></param>
        /// <returns></returns>
        private List<Vector2> CoordGenerator(String[][] layout)
        {
            List<Vector2> startCoords = new List<Vector2>();
            
            //Check for Up and Down coords
            for (int i = 0; i < layout.Length; i++)
            {
                if (layout[0][i] == "D")
                {
                    Vector2 newVec = new Vector2(i, 0);
                    startCoords.Add(newVec);
                }
                if (layout[0][i] == "U")
                {
                    Vector2 newVec = new Vector2(i, layout[0].Length-1);
                    startCoords.Add(newVec);
                }          
            }

            //Check for Right and Left
            for (int i = 0; i < layout.Length; i++)
            {
                if (layout[i][0] == "R")
                {
                    Vector2 newVec = new Vector2(0, i);
                    startCoords.Add(newVec);
                }
                if (layout[i][0] == "L")
                {
                    Vector2 newVec = new Vector2(layout[0].Length-1, i);
                    startCoords.Add(newVec);
                }
            }
            return startCoords;
        }
        
        /// <summary>
        /// The method will be in charge of randomly choosing which type of vehicle and if it's electric or not.
        /// Once all the total vehicles are created, it will stop instantiating new vehicles.
        /// </summary>
        private void createVehicle()
        {
            int rndVehicleTypeVal = random.Next(1, 101); 
            int rndElectricVal = random.Next(1, 101); 
            IVehicle newVehicle = null;
            
            if ((this.numOfElecCars + this.numOfElecCycle + this.numOfGasCars + this.numOfGasCycle) > 0)
            {
                //Instantiate a car if we haven't reached the total amount of cars
                if (1 <= rndVehicleTypeVal && rndVehicleTypeVal <= this.percentCars && (this.numOfGasCars + this.numOfElecCars > 0))
                {
                    //Create Gas Car based on the random# and if we haven't reached the limit of Gas cars || If all Electric Car were instantiated, just create a Gas Car
                    if ((rndElectricVal > this.percentElectric && rndElectricVal <= 100 && this.numOfGasCars > 0) || (this.numOfElecCars == 0))
                    {
                        newVehicle = new Car(this.Grid);
                        this.numOfGasCars--;
                    }
                    //Create Electric Car based on the random# and if we haven't reached the limit of Electric cars
                    else
                    {
                        newVehicle = new Car(this.Grid);
                        newVehicle = new Electric(newVehicle);
                        this.numOfElecCars--;
                    }
                }
                //Instantiate motorcycle if we haven't reached the total amount of motorcycles
                else
                {
                    //Create Gas Motorcycle based on the random# and if we haven't reached the limit of Gas cars || If all Electric Motorcycle were instantiated, just create a Gas Motorcycle
                    if ((rndElectricVal > this.percentElectric && rndElectricVal <= 100 && this.numOfGasCycle > 0) || (this.numOfElecCycle == 0))
                    {
                        newVehicle = new Motorcycle(this.Grid);
                        this.numOfGasCycle--;
                    }
                    //Create Electric Motorcycle based on the random# and if we haven't reached the limit of Electric cars
                    else
                    {
                        newVehicle = new Motorcycle(this.Grid);
                        newVehicle = new Electric(newVehicle);
                        this.numOfElecCycle--;
                    }
                }

                //Subscribe events
                newVehicle.Done += this.Total.VehicleOver;
                newVehicle.Moved += this.Total.Moved;
                newVehicle.Waiting += this.Total.Waiting;
                
                //Add to Intersection for it to place the vehicle
                this.Intersection.Add(newVehicle);
            }
        }

    }
}
