﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traffic
{
    public abstract class Vehicle : IVehicle
    {
        //Fields
        private Grid grid;
        //Properties
        public virtual Direction Direction { get; set; }
        public virtual int X { get; set; }
        public virtual int Y { get; set; }
        public int Passengers { get; set; }
        public double EmissionIdle { get; set; }
        public double EmissionMoving { get; set; }

        public Vehicle(double emissionMoving, double emissionIdle, int passengers, Grid grid)
        {
            this.EmissionMoving = emissionMoving;
            this.EmissionIdle = emissionIdle;
            this.Passengers = passengers;
            this.grid = grid;
        }

        //Events
        public event VehicleActionEventHandler Done;
        public event VehicleActionEventHandler Moved;
        public event VehicleActionEventHandler Waiting;

        
        /// <summary>
        /// It checkes the vehicle's coordinate in grid to find if it's already in intersection then it returns true, 
        /// otherwise it returns false.
        /// </summary>
        /// <returns>boolean</returns>
        public virtual bool InIntersection()
        {
            if (grid.InBounds(this.X, this.Y) && grid[this.X, this.Y] is IntersectionTile)
                return true;
            return false;
        }
        /// <summary>
        /// Verifies the light and the vehicle position in grid. If the vehicle is in bounds it invokes the Done event.
        /// If light is green or if the vehicle is already in intersection or if the next tile in vehicle's direction 
        /// is not the intersection and it's free it will move the vehicle and fires the move event.
        /// Otherwise if the light is amber or red and the next tile in vehicle's direction is intersection it will stop and 
        /// raises the waiting event.
        /// </summary>
        /// <param name="signal"></param>

        public virtual void Move(ISignalStrategy signal)
        {
            Colour colour = signal.GetColour(Direction);
            if (InIntersection() || !NextIsIntersection() || colour == Colour.GREEN)
            {
                switch (this.Direction)
                {
                    case Direction.DOWN:

                        //Ckecks the next tile to be free
                        if (grid.InBounds(this.X, this.Y + 1) && !grid.IsOccupied(this.X, this.Y + 1))
                        {
                            grid[this.X, this.Y].Occupied = false;
                            grid[this.X, this.Y + 1].Occupied = true;
                            this.Y += 1;
                            Moved?.Invoke(this);
                        }
                        else if (!grid.InBounds(this.X, this.Y + 1))
                        {
                            grid[this.X, this.Y].Occupied = false;
                            Done?.Invoke(this);
                        }
                        break;

                    case Direction.UP:

                        if (grid.InBounds(this.X, this.Y - 1) && !grid.IsOccupied(this.X, this.Y - 1))
                        {

                            grid[this.X, this.Y].Occupied = false;
                            grid[this.X, this.Y - 1].Occupied = true;
                            this.Y -= 1;
                            Moved?.Invoke(this);
                        }
                        else if (!grid.InBounds(this.X, this.Y - 1))
                        {
                            grid[this.X, this.Y].Occupied = false;
                            Done?.Invoke(this);
                        }
                        break;

                    case Direction.RIGHT:

                        if (grid.InBounds(this.X + 1, this.Y) && !grid.IsOccupied(this.X + 1, this.Y))
                        {

                            grid[this.X, this.Y].Occupied = false;
                            grid[this.X + 1, this.Y].Occupied = true;
                            this.X += 1;
                            Moved?.Invoke(this);
                        }
                        else if (!grid.InBounds(this.X + 1, this.Y))
                        {
                            grid[this.X, this.Y].Occupied = false;
                            Done?.Invoke(this);
                        }
                        break;

                    case Direction.LEFT:

                        if (grid.InBounds(this.X - 1, this.Y) && !grid.IsOccupied(this.X - 1, this.Y))
                        {
                            grid[this.X, this.Y].Occupied = false;
                            grid[this.X - 1, this.Y].Occupied = true;
                            this.X -= 1;
                            Moved?.Invoke(this);
                        }
                        else if (!grid.InBounds(this.X - 1, this.Y))
                        {
                            grid[this.X, this.Y].Occupied = false;
                            Done?.Invoke(this);

                        }
                        break;

                }//End switch
            }
            else
            {
                Waiting?.Invoke(this);
            }
        }//End Move()


        /// <summary>
        /// This checks the next tile in the vehicle's direction to determine if it's intersection or not.
        /// </summary>
        /// <returns> boolean</returns>
        public virtual bool NextIsIntersection()
        {
            switch (this.Direction)
            {
                case Direction.UP:
                {
                   if (grid.InBounds(this.X, this.Y - 1) && grid[this.X, this.Y - 1] is IntersectionTile)
                       return true;
                   break;
                }
                case Direction.DOWN:
                {
                    if (grid.InBounds(this.X, this.Y + 1) && grid[this.X, this.Y + 1] is IntersectionTile)
                        return true;
                    break;
                }
                case Direction.RIGHT:
                {
                    if (grid.InBounds(this.X + 1, this.Y) && grid[this.X + 1, this.Y] is IntersectionTile)
                        return true;
                    break;
                }
                case Direction.LEFT:
                {
                    if (grid.InBounds(this.X - 1, this.Y) && grid[this.X - 1, this.Y] is IntersectionTile)
                        return true;
                    break;
                }
            }
            return false;
        }//End NextIsIntersection()
    }//End Vehicle 
}
