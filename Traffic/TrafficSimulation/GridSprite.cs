﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traffic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace TrafficSimulation
{
    /// <summary>
    /// Class representing the DrawableGameComponent GridSprite
    /// Handles drawing the tiles of the Intersection
    /// </summary>
    class GridSprite : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Simulation simulation;
        private Texture2D imageGrass;
        private Texture2D imageIntersection;
        private Texture2D imageRRoad;
        private Texture2D imageLRoad;
        private Texture2D imageURoad;
        private Texture2D imageDRoad;
        private Texture2D imageRed;
        private Texture2D imageGreen;
        private Texture2D imageYellow;

        /// <summary>
        /// Constructor for GridSprite
        /// </summary>
        /// <param name="simulation"></param>
        public GridSprite(Simulation simulation) : base(simulation)
        {
            this.simulation = simulation;
        }

        /// <summary>
        /// Initialize method
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// loads images and sets up the SpriteBatch
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            imageGrass = simulation.Content.Load<Texture2D>(@"images\grass");
            imageIntersection = simulation.Content.Load<Texture2D>(@"images\intersection");
            imageRRoad = simulation.Content.Load<Texture2D>(@"images\roadright");
            imageLRoad = simulation.Content.Load<Texture2D>(@"images\roadleft");
            imageURoad = simulation.Content.Load<Texture2D>(@"images\roadup");
            imageDRoad = simulation.Content.Load<Texture2D>(@"images\roaddown");
            imageRed = simulation.Content.Load<Texture2D>(@"images\red");
            imageGreen = simulation.Content.Load<Texture2D>(@"images\green");
            imageYellow = simulation.Content.Load<Texture2D>(@"images\yellow");
            base.LoadContent();
        }

        /// <summary>
        /// Called 60 times per second
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws tiles of intersection
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            for (int x = 0; x < simulation.tc.Grid.Size; x++)
            {
                for(int y = 0; y < simulation.tc.Grid.Size; y++)
                {
                    spriteBatch.Draw(TextureFinder(x,y), new Rectangle(x * 30, y * 30, 30, 30), Color.White);
                }
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Find which texture to set for the given coordinate
        /// </summary>
        /// <param name="x">x-coordinate</param>
        /// <param name="y">y-coordinate</param>
        /// <returns>Appropriate Texture2D for given coordinate</returns>
        private Texture2D TextureFinder(int x, int y)
        {
            switch(simulation.tc.Grid[x, y])
            {
                case IntersectionTile i:
                    return imageIntersection;
                case Road r:
                    switch (r.Direction)
                    {
                        case Direction.DOWN:
                            return imageDRoad;
                        case Direction.LEFT:
                            return imageLRoad;
                        case Direction.RIGHT:
                            return imageRRoad;
                        default:
                            return imageURoad;
                    }
                case Light l:
                    switch (l.Colour)
                    {
                        case Colour.GREEN:
                            return imageGreen;
                        case Colour.AMBER:
                            return imageYellow;
                        default:
                            return imageRed;
                    }
                default:
                    return imageGrass;
            }
        }
    }
}
