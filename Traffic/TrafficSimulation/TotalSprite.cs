﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Traffic;


namespace TrafficSimulation
{
    public class TotalSprite : DrawableGameComponent
    {
        private Simulation game;
        private SpriteBatch spriteBatch;
        private SpriteFont trafficFont;
        private Vector2 emissionPos;
        private Vector2 passengerPos;
        public TotalSprite(Simulation game) : base(game)
        {
            this.game = game;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            trafficFont = game.Content.Load<SpriteFont>("TrafficFont");
            //check for empty space on bottom or top to draw the text.
            if (GraphicsDevice.Viewport.Height >= (game.tc.Grid.Size + 3) * 30)
            {
                emissionPos = new Vector2(30, (game.tc.Grid.Size + 1) * 30);
                passengerPos = new Vector2(30, (game.tc.Grid.Size + 2) * 30);
                Console.WriteLine(emissionPos.ToString());
            }
            else
            {
                emissionPos = new Vector2(30, 0);
                passengerPos = new Vector2(30, 30);
            }


            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }


        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(trafficFont, "Total Emissions: " + game.tc.Total.Emission, emissionPos, Color.White);
            spriteBatch.DrawString(trafficFont, "Total Passengers: " + game.tc.Total.Passengers, passengerPos, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
