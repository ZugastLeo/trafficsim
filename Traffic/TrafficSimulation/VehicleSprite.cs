﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traffic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace TrafficSimulation
{
    public class VehicleSprite: DrawableGameComponent 
    {
        private Simulation game;
        private SpriteBatch spriteBatch;
        private Texture2D carLeftImage;
        private Texture2D carRightImage;
        private Texture2D carUpImage;
        private Texture2D carDownImage;
        private Texture2D motoLeftImage;
        private Texture2D motoRightImage;
        private Texture2D motoUpImage;
        private Texture2D motoDownImage;
        private int updateFrequency = 5;
        private int updateCounter = 0;

        /// <summary>
        /// Creates a VehicleSprite object that will draw the vehicles on the Simulation's Grid
        /// </summary>
        /// <param name="game"></param>
        public VehicleSprite(Simulation game) : base(game)
        {
            this.game = game;
        }

        /// <summary>
        /// Initialize method
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Loads the sprites for the cars and motorcycles
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            carLeftImage = game.Content.Load<Texture2D>(@"images\carLeft");
            carRightImage = game.Content.Load<Texture2D>(@"images\carRight");
            carUpImage = game.Content.Load<Texture2D>(@"images\carUp");
            carDownImage = game.Content.Load<Texture2D>(@"images\carDown");
            motoLeftImage = game.Content.Load<Texture2D>(@"images\motorcycleLeft");
            motoRightImage = game.Content.Load<Texture2D>(@"images\motorcycleRight");
            motoUpImage = game.Content.Load<Texture2D>(@"images\motorcycleUp");
            motoDownImage = game.Content.Load<Texture2D>(@"images\motorcycleDown");
            base.LoadContent();
        }

        /// <summary>
        /// Uses the intersection and trafficcontrol update methods to move the cars
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (updateCounter == updateFrequency) { 
                game.tc.Intersection.Update();
                game.tc.Update();
                base.Update(gameTime);
                updateCounter = 0;
            }
            updateCounter++;
        }

        /// <summary>
        /// Draws the vehicle types according to their class and their direction
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            //Go through all vehicles in the grid
            foreach (IVehicle vehicle in game.tc.Intersection)
            {
                //Choose which sprite to draw and its position
                if (vehicle is Car)
                {
                    switch (vehicle.Direction)
                    {
                        case Direction.DOWN:
                            spriteBatch.Draw(carDownImage, new Vector2(vehicle.X*30, vehicle.Y*30), Color.White);
                            break;
                        case Direction.LEFT:
                            spriteBatch.Draw(carLeftImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                        case Direction.RIGHT:
                            spriteBatch.Draw(carRightImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                        case Direction.UP:
                            spriteBatch.Draw(carUpImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                    }
                }
                else if(vehicle is Motorcycle)
                {
                    switch (vehicle.Direction)
                    {
                        case Direction.DOWN:
                            spriteBatch.Draw(motoDownImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                        case Direction.LEFT:
                            spriteBatch.Draw(motoLeftImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                        case Direction.RIGHT:
                            spriteBatch.Draw(motoRightImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                        case Direction.UP:
                            spriteBatch.Draw(motoUpImage, new Vector2(vehicle.X * 30, vehicle.Y * 30), Color.White);
                            break;
                    }
                }  
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
