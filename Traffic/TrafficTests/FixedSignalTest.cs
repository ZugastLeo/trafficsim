﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace TrafficTests
{
    [TestClass]
    public class FixedSignalTest
    {
        [TestMethod]
        public void GetColourTest()
        {
            //Arrange
            FixedSignal newSig = new FixedSignal(20, 5, 10, 5);

            //Act & Assert
            Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.RIGHT));
            Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.LEFT));
            Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.UP));
            Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.DOWN));
        }

        [TestMethod]
        public void UpdateTest()
        {
            //Arrange
            FixedSignal newSig = new FixedSignal(20, 5, 10, 5);

            //Act & Assert
            for (int i = 1; i < 45; i++)
            {
                if (i > 40)
                {
                    Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.RIGHT));
                    Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.LEFT));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.UP));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.DOWN));
                }
                else if (i > 35)
                {
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.RIGHT));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.LEFT));
                    Assert.AreEqual(Colour.AMBER, newSig.GetColour(Direction.UP));
                    Assert.AreEqual(Colour.AMBER, newSig.GetColour(Direction.DOWN));
                }
                else if (i > 25)
                {
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.RIGHT));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.LEFT));
                    Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.UP));
                    Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.DOWN));
                }
                else if (i > 20)
                {
                    Assert.AreEqual(Colour.AMBER, newSig.GetColour(Direction.RIGHT));
                    Assert.AreEqual(Colour.AMBER, newSig.GetColour(Direction.LEFT));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.UP));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.DOWN));
                }
                else if (i > 0)
                {
                    Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.RIGHT));
                    Assert.AreEqual(Colour.GREEN, newSig.GetColour(Direction.LEFT));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.UP));
                    Assert.AreEqual(Colour.RED, newSig.GetColour(Direction.DOWN));
                }
                newSig.Update();
            }

        }
    }
}