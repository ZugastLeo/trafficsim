﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;

namespace TrafficTests
{
    [TestClass]
    public class GridTest
    {
        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void TestException()
        {
            Tile[,] t = new Tile[2, 2];
            Tile m = t[4, 4];
        }


        [TestMethod]
        public void TestIsOccupiedTrue()
        {
            Tile[,] t = new Tile[2, 2];

            for (int i = 0; i < t.GetLength(0); i++)
            {
                for (int j = 0; j < t.GetLength(1); j++)
                {
                    t[i, j] = new Grass();
                }
            }
            Grid g = new Grid(t);
            g[1, 1].Occupied = true;
            bool expected = true;
            bool actual = g.IsOccupied(1, 1);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestIsOccupiedFalse()
        {
            Tile[,] t = new Tile[2, 2];

            for (int i = 0; i < t.GetLength(0); i++)
            {
                for (int j = 0; j < t.GetLength(1); j++)
                {
                    t[i, j] = new Road(Direction.LEFT);
                }
            }
            Grid g = new Grid(t);

            bool expected = false;
            bool actual = g.IsOccupied(1, 0);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestInBoundsTrue()
        {
            Tile[,] t = new Tile[2, 2];

            for (int i = 0; i < t.GetLength(0); i++)
            {
                for (int j = 0; j < t.GetLength(1); j++)
                {
                    t[i, j] = new Grass();
                }
            }
            Grid g = new Grid(t);

            bool expected = true;
            bool actual = g.InBounds(0, 0);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestInBoundsFalse()
        {
            Tile[,] t = new Tile[2, 2];

            for (int i = 0; i < t.GetLength(0); i++)
            {
                for (int j = 0; j < t.GetLength(1); j++)
                {
                    t[i, j] = new Road(Direction.UP);
                }
            }
            Grid g = new Grid(t);

            bool expected = false;
            bool actual = g.InBounds(2, 3);
            Assert.AreEqual(expected, actual);
        }
    }
}
