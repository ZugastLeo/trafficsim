﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace TrafficTests
{
    [TestClass]
    public class IntersectionTest
    {
        
        [TestMethod]
        public void AddTest()
        {
            //Arrange 
            Intersection inter = new Intersection(F(), getCoordsList(), createGrid());
            Car nCar = createCar();
            int counter = 0;
            int counter2 = 0;

            //Act
            inter.Add(nCar);
            foreach (var veh in inter)
            {
                counter++;
            }

            inter.Add(createCar());
            foreach (var veh in inter)
            {
                counter2++;
            }
            //Assert
            Assert.AreEqual(1, counter);
            Assert.AreEqual(2, counter2);

        }
        
        [TestMethod]
        public void UpdateTest()
        {
            //Arrange
            Intersection inter = new Intersection(F(), getCoordsList(), createGrid());
            Car nCar = createCar();
            int x = 0;
            int y = 0;

            //Act
            inter.Add(nCar);
            Direction nDirection = nCar.Direction;
            x = nCar.X;
            y = nCar.Y;

            inter.Update();

            //Assert
            switch (nDirection)
            {
                case Direction.UP:
                case Direction.DOWN:
                    Assert.AreNotEqual(y, nCar.Y);
                    Assert.AreEqual(x, nCar.X);
                    break;
                case Direction.RIGHT:
                case Direction.LEFT:
                    Assert.AreNotEqual(x, nCar.X);
                    Assert.AreEqual(y, nCar.Y);
                    break;
            }
        }

        [TestMethod]
        public void removeTest()
        {
            //Arrange
            List<Vector2> leftRoad = new List<Vector2>();
            List<Vector2> rightRoad = new List<Vector2>();
            leftRoad.Add(new Vector2(5, 2));
            rightRoad.Add(new Vector2(0, 3));
            Intersection inter = new Intersection(F(), leftRoad, createGrid());
            Intersection inter2 = new Intersection(F(), rightRoad, createGrid());
            Car nCar = createCar();
            Car nCar2 = createCar();
            int addCount = 0;
            int removeCount = 0;
            int addCount2 = 0;
            int removeCount2 = 0;

            //Act
            inter.Add(nCar);
            inter2.Add(nCar2);

            //Left Road
            foreach (var veh in inter)
            {
                addCount++;
            }

            //Force car out of bounds
            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine(nCar.X + " " + nCar.Y + " " + nCar.Direction);
                inter.Update();
            }

            foreach (var veh in inter)
            {
                removeCount++;
            }


            //Right Road
            foreach (var veh in inter2)
            {
                addCount2++;
            }

            //Force car out of bounds
            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine(nCar2.X + " " + nCar2.Y + " " + nCar2.Direction);
                inter2.Update();
            }

            foreach (var veh in inter)
            {
                removeCount2++;
            }

            //Assert
            Assert.AreEqual(1, addCount);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(1, addCount2);
            Assert.AreEqual(0, removeCount2);
        }

        private Car createCar()
        {
            return new Car(createGrid());
        }

        private List<Vector2> getCoordsList()
        {
            Vector2 d = new Vector2(2, 0);
            Vector2 r = new Vector2(0, 3);
            Vector2 u = new Vector2(3, 5);
            Vector2 l = new Vector2(5, 2);
            List<Vector2> list = new List<Vector2>();
            list.Add(d);
            list.Add(r);
            list.Add(u);
            list.Add(l);
            return list;
        }


        private Grid createGrid()
        {
            Tile[,] tileLayout = { { G(), G(), R(Direction.LEFT), R(Direction.RIGHT), G(), G()},
                                   { G(), L(Direction.DOWN,F()), R(Direction.LEFT), R(Direction.RIGHT), L(Direction.RIGHT,F()), G()},
                                   { R(Direction.DOWN),R(Direction.DOWN),I(),I(),R(Direction.DOWN),R(Direction.DOWN)},
                                   { R(Direction.UP),R(Direction.UP),I(),I(),R(Direction.UP),R(Direction.UP)},
                                   { G(), L(Direction.LEFT,F()), R(Direction.LEFT), R(Direction.RIGHT), L(Direction.UP,F()), G()},
                                   { G(), G(), R(Direction.LEFT), R(Direction.RIGHT), G(), G()}};
            return new Grid(tileLayout);
        }

        private static FixedSignal F()
        {
            return new FixedSignal(20, 5, 10, 5);
        }
        private static Grass G()
        {
            return new Grass();
        }

        private static IntersectionTile I()
        {
            return new IntersectionTile();
        }

        private static Light L(Direction d, FixedSignal s)
        {
            return new Light(s, d);
        }

        private static Road R(Direction d)
        {
            return new Road(d);
        }
    }
}