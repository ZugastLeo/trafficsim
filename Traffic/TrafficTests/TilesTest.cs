﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;

namespace TrafficTests
{
    [TestClass]
    public class TilesTests
    {
        [TestMethod]
        public void TestGrassConstructor()
        {
            //Arrange & Act
            Tile g = new Grass();

            //Assert
            Assert.AreEqual(Direction.NONE, g.Direction);
        }

        [TestMethod]
        public void TestIntersectionConstructor()
        {
            //Arrange & Act
            Tile i = new IntersectionTile();

            //Assert
            Assert.AreEqual(Direction.NONE, i.Direction);
        }

        [TestMethod]
        public void TestRoadConstructor()
        {
            //Arrange & Act
            Tile r = new Road(Direction.RIGHT);
            Direction d = Direction.RIGHT;
            //Assert
            Assert.AreEqual(d, r.Direction);
        }

        [TestMethod]
        public void TestLightConstructor()
        {
            //Arrange & Act
            Light l = new Light(new FixedSignal(20, 5, 10, 5), Direction.RIGHT);
            Direction d = Direction.RIGHT;
            Colour c = Colour.GREEN;
            //Assert
            Assert.AreEqual(d, l.Direction);
            Assert.AreEqual(c, l.Colour);
        }
    }
}
