﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;

namespace TrafficTests
{
    [TestClass]
    public class TotalTest
    {
        [TestMethod]
        public void TestVehicleOver()
        {
            //Arrange
            IVehicle v = createCar();
            IVehicle m = createMotorcycle();
            Total t = new Total();

            //Act
            t.VehicleOver(v);
            t.VehicleOver(m);
            t.VehicleOver(v);

            //Assert
            Assert.AreEqual(7, t.Passengers);
        }

        [TestMethod]
        public void TestMoved()
        {
            //Arrange
            IVehicle v = createCar();
            IVehicle m = createMotorcycle();
            Total t = new Total();

            //Act
            t.Moved(v);
            t.Moved(m);
            t.Moved(v);

            //Assert
            Assert.AreEqual(12, t.Emission);
        }

        [TestMethod]
        public void TestWaiting()
        {
            //Arrange
            IVehicle v = createCar();
            IVehicle m = createMotorcycle();
            Total t = new Total();

            //Act
            t.Waiting(v);
            t.Waiting(m);
            t.Waiting(v);

            //Assert
            Assert.AreEqual(5, t.Emission);            
        }

        private Car createCar()
        {
            return new Car(createGrid());
        }

        private Motorcycle createMotorcycle()
        {
            return new Motorcycle(createGrid());
        }

        private Grid createGrid()
        {
            Tile[,] tiles = { { G(), G(), R(Direction.DOWN), R(Direction.UP), G(), G() },
                              { G(), L(Direction.DOWN,F()), R(Direction.DOWN), R(Direction.UP), L(Direction.LEFT,F()), G() },
                              { R(Direction.LEFT), R(Direction.LEFT), I(), I(), R(Direction.LEFT), R(Direction.LEFT)},
                              { R(Direction.RIGHT), R(Direction.RIGHT), I(), I(), R(Direction.RIGHT), R(Direction.RIGHT)},
                              { G(), L(Direction.UP,F()), R(Direction.DOWN), R(Direction.UP), L(Direction.RIGHT,F()), G() },
                              { G(), G(), R(Direction.DOWN), R(Direction.UP), G(), G() } };
            return new Grid(tiles);
        }

        private static FixedSignal F()
        {
            return new FixedSignal(20, 5, 10, 5);
        }

        private static Grass G()
        {
            return new Grass();
        }

        private static IntersectionTile I()
        {
            return new IntersectionTile();
        }

        private static Light L(Direction d, FixedSignal s)
        {
            return new Light(s, d);
        }

        private static Road R(Direction d)
        {
            return new Road(d);
        }
    }
}
