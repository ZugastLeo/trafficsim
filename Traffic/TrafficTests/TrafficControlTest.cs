﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;

namespace TrafficTests
{
    [TestClass]
    public class TrafficControlTest
    {
        [TestMethod]
        public void TestParse()
        {
            //Arrange
            TrafficControl tc = new TrafficControl();
            string fileContent =
                     "100" + Environment.NewLine +
                     "2" + Environment.NewLine +
                     "80" + Environment.NewLine +
                     "10" + Environment.NewLine +
                     "20 5 10 5" + Environment.NewLine +
                     "G G D U G G" + Environment.NewLine +
                     "G 1 D U 2 G" + Environment.NewLine +
                     "L L I I L L" + Environment.NewLine +
                     "R R I I R R" + Environment.NewLine +
                     "G 3 D U 4 G" + Environment.NewLine +
                     "G G D U G G";
            Tile[,] tiles = { { G(), G(), R(Direction.DOWN), R(Direction.UP), G(), G() },
                              { G(), L(Direction.DOWN,F()), R(Direction.DOWN), R(Direction.UP), L(Direction.LEFT,F()), G() },
                              { R(Direction.LEFT), R(Direction.LEFT), I(), I(), R(Direction.LEFT), R(Direction.LEFT)},
                              { R(Direction.RIGHT), R(Direction.RIGHT), I(), I(), R(Direction.RIGHT), R(Direction.RIGHT)},
                              { G(), L(Direction.UP,F()), R(Direction.DOWN), R(Direction.UP), L(Direction.RIGHT,F()), G() },
                              { G(), G(), R(Direction.DOWN), R(Direction.UP), G(), G() }};
            //Act
            tc.Parse(fileContent);

            //Assert
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Assert.AreEqual(tiles[i, j].GetType(), tc.Grid[i, j].GetType());
                }
            }
        }
        
        [TestMethod]
        public void TestUpdate()
        {
            //Arrange
            TrafficControl tc = new TrafficControl();
            string fileContent =
                     "100" + Environment.NewLine +
                     "2" + Environment.NewLine +
                     "80" + Environment.NewLine +
                     "10" + Environment.NewLine +
                     "20 5 10 5" + Environment.NewLine +
                     "G G D U G G" + Environment.NewLine +
                     "G 1 D U 2 G" + Environment.NewLine +
                     "L L I I L L" + Environment.NewLine +
                     "R R I I R R" + Environment.NewLine +
                     "G 3 D U 4 G" + Environment.NewLine +
                     "G G D U G G";
            tc.Parse(fileContent);

            //Act
            tc.Update();
            tc.Update();
            tc.Update();
            tc.Update();

            int counter = 0;
            foreach (IVehicle v in tc.Intersection)
            {
                counter++;
            }

            //Assert
            Assert.AreEqual(2, counter);
        }
        
        private static FixedSignal F()
        {
            return new FixedSignal(20, 5, 10, 5);
        }

        private static Grass G()
        {
            return new Grass();
        }

        private static IntersectionTile I()
        {
            return new IntersectionTile();
        }

        private static Light L(Direction d, FixedSignal s)
        {
            return new Light(s, d);
        }

        private static Road R(Direction d)
        {
            return new Road(d);
        }
    }
}
