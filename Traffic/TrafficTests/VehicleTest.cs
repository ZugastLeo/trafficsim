﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Traffic;

namespace TrafficTests
{
    [TestClass]
    public class VehicleTest
    {
        [TestMethod]
        public void TestCar()
        {
            //Arrange
            Grass grass = new Grass();
            Road rup = new Road(Direction.UP);
            Road rdown = new Road(Direction.DOWN);
            Road rleft = new Road(Direction.LEFT);
            Road rright = new Road(Direction.RIGHT);
            IntersectionTile it = new IntersectionTile();
            Tile[,] tileArray = { { grass, rdown, rup, grass },
                           { rleft, it, it, rleft },
                           { rright, it, it, rright },
                           { grass, rdown, rup, grass} };
            Grid simpleGrid = new Grid(tileArray);
            //Act
            Car toECar = new Car(simpleGrid);
            Electric electricCar = new Electric(toECar);
            Car car = new Car(simpleGrid);
            electricCar.X = 2;
            electricCar.Y = 1;
            car.X = 2;
            car.Y = 1;
            electricCar.Direction = Direction.NONE;
            car.Direction = Direction.NONE;
            //Assert

            //Test Car Properties & Constructor
            Assert.AreEqual(5, car.EmissionMoving);
            Assert.AreEqual(2, car.EmissionIdle);
            Assert.AreEqual(3, car.Passengers);
            Assert.AreEqual(2, car.X);
            Assert.AreEqual(1, car.Y);
            Assert.AreEqual(Direction.NONE, car.Direction);
            //Test Electric Car Properties & Constructor
            Assert.AreEqual(1.25, electricCar.EmissionMoving);
            Assert.AreEqual(0, electricCar.EmissionIdle);
            Console.WriteLine(electricCar.Passengers);
            Assert.AreEqual(3, electricCar.Passengers);
            Assert.AreEqual(2, electricCar.X);
            Assert.AreEqual(1, electricCar.Y);
            Assert.AreEqual(Direction.NONE, electricCar.Direction);
        }

        [TestMethod]
        public void TestMotorcycle()
        {
            //Arrange
            Grass grass = new Grass();
            Road rup = new Road(Direction.UP);
            Road rdown = new Road(Direction.DOWN);
            Road rleft = new Road(Direction.LEFT);
            Road rright = new Road(Direction.RIGHT);
            IntersectionTile it = new IntersectionTile();
            Tile[,] tileArray = { { grass, rdown, rup, grass },
                           { rleft, it, it, rleft },
                           { rright, it, it, rright },
                           { grass, rdown, rup, grass} };
            Grid simpleGrid = new Grid(tileArray);
            //Act
            Motorcycle toEMoto = new Motorcycle(simpleGrid);
            Electric electricMoto = new Electric(toEMoto);
            Motorcycle moto = new Motorcycle(simpleGrid);
            electricMoto.X = 2;
            electricMoto.Y = 1;
            moto.X = 2;
            moto.Y = 1;
            electricMoto.Direction = Direction.NONE;
            moto.Direction = Direction.NONE;
            //Assert

            //Test Moto Properties & Constructor
            Assert.AreEqual(2, moto.EmissionMoving);
            Assert.AreEqual(1, moto.EmissionIdle);
            Assert.AreEqual(1, moto.Passengers);
            Assert.AreEqual(2, moto.X);
            Assert.AreEqual(1, moto.Y);
            Assert.AreEqual(Direction.NONE, moto.Direction);
            //Test Electric Moto Properties & Constructor
            Assert.AreEqual(0.5, electricMoto.EmissionMoving);
            Assert.AreEqual(0, electricMoto.EmissionIdle);
            Assert.AreEqual(1, electricMoto.Passengers);
            Assert.AreEqual(2, electricMoto.X);
            Assert.AreEqual(1, electricMoto.Y);
            Assert.AreEqual(Direction.NONE, electricMoto.Direction);
        }

        [TestMethod]
        public void TestNextIsIntersection()
        {
            //Arrange
            Grass grass = new Grass();
            Road rup = new Road(Direction.UP);
            Road rdown = new Road(Direction.DOWN);
            Road rleft = new Road(Direction.LEFT);
            Road rright = new Road(Direction.RIGHT);
            IntersectionTile it = new IntersectionTile();
            Tile[,] tileArray = { { grass, rdown, rup, grass },
                           { rleft, it, it, rleft },
                           { rright, it, it, rright },
                           { grass, rdown, rup, grass} };
            Grid simpleGrid = new Grid(tileArray);
            Car rightCar = new Car(simpleGrid);
            Car leftCar = new Car(simpleGrid);
            Car upCar = new Car(simpleGrid);
            Car downCar = new Car(simpleGrid);
            //Act 
            rightCar.Direction = Direction.RIGHT;
            rightCar.X = 0;
            rightCar.Y = 2;
            leftCar.Direction = Direction.LEFT;
            leftCar.X = 3;
            leftCar.Y = 1;
            upCar.Direction = Direction.UP;
            upCar.X = 2;
            upCar.Y = 3;
            downCar.Direction = Direction.DOWN;
            downCar.X = 1;
            downCar.Y = 0;
            //Assert
            //All directions pointed towards intersections
            Assert.IsTrue(leftCar.NextIsIntersection());
            Assert.IsTrue(rightCar.NextIsIntersection());
            Assert.IsTrue(upCar.NextIsIntersection());
            Assert.IsTrue(downCar.NextIsIntersection());
            rightCar.Direction = Direction.LEFT;
            leftCar.Direction = Direction.RIGHT;
            upCar.Direction = Direction.DOWN;
            downCar.Direction = Direction.UP;
            //All directions pointed outside of bounds
            Assert.IsFalse(leftCar.NextIsIntersection());
            Assert.IsFalse(rightCar.NextIsIntersection());
            Assert.IsFalse(upCar.NextIsIntersection());
            Assert.IsFalse(downCar.NextIsIntersection());
        }

        [TestMethod]
        public void TestInIntersection()
        {
            //Arrange
            Grass grass = new Grass();
            Road rup = new Road(Direction.UP);
            Road rdown = new Road(Direction.DOWN);
            Road rleft = new Road(Direction.LEFT);
            Road rright = new Road(Direction.RIGHT);
            IntersectionTile it = new IntersectionTile();
            Tile[,] tileArray = { { grass, rdown, rup, grass },
                           { rleft, it, it, rleft },
                           { rright, it, it, rright },
                           { grass, rdown, rup, grass} };
            Grid simpleGrid = new Grid(tileArray);
            Car insideInterCar = new Car(simpleGrid);
            Car outsideInterCar = new Car(simpleGrid);
            Car outsideLBoundsCar = new Car(simpleGrid);
            Car outsideHBoundsCar = new Car(simpleGrid);
            Car inGrassCar = new Car(simpleGrid);
            //Act 
            insideInterCar.X = 1;
            insideInterCar.Y = 1;
            outsideInterCar.X = 2;
            outsideInterCar.Y = 0;
            outsideLBoundsCar.X = -12345;
            outsideLBoundsCar.Y = 0;
            outsideHBoundsCar.X = 0;
            outsideHBoundsCar.Y = 12345;
            inGrassCar.X = 0;
            inGrassCar.Y = 0;
            //Assert
            //Inside intersection
            Assert.IsTrue(insideInterCar.InIntersection());
            //All directions pointed outside of bounds
            Assert.IsFalse(outsideInterCar.InIntersection());
            Assert.IsFalse(outsideLBoundsCar.InIntersection());
            Assert.IsFalse(outsideHBoundsCar.InIntersection());
            Assert.IsFalse(inGrassCar.InIntersection());
        }

        [TestMethod]
        public void TestMove()
        {
            //Arrange
            #region
            FixedSignal fs = new FixedSignal(4, 2, 4, 2);
            Light ulight = new Light(fs, Direction.UP);
            Light dlight = new Light(fs, Direction.DOWN);
            Light llight = new Light(fs, Direction.LEFT);
            Light rlight = new Light(fs, Direction.RIGHT);
            Grass grass = new Grass();
            Road rup1 = new Road(Direction.UP);
            Road rup2 = new Road(Direction.UP);
            Road rdown1 = new Road(Direction.DOWN);
            Road rdown2 = new Road(Direction.DOWN);
            Road rleft1 = new Road(Direction.LEFT);
            Road rleft2 = new Road(Direction.LEFT);
            Road rright1 = new Road(Direction.RIGHT);
            Road rright2 = new Road(Direction.RIGHT);
            IntersectionTile it1 = new IntersectionTile();
            IntersectionTile it2 = new IntersectionTile();
            IntersectionTile it3 = new IntersectionTile();
            IntersectionTile it4 = new IntersectionTile();
            #endregion
            Tile[,] tileArray = { { llight, rright1, rleft1, dlight},
                                  { rup2, it1, it3, rup1},
                                  { rdown2, it2, it4, rdown1},
                                  { ulight, rright2 , rleft2, rlight} };
            Grid simpleGrid = new Grid(tileArray);
            Car rightCar = new Car(simpleGrid);
            Car leftCar = new Car(simpleGrid);
            Car upCar = new Car(simpleGrid);
            Car downCar = new Car(simpleGrid);
            //Act 
            rightCar.Direction = Direction.RIGHT;
            rightCar.X = 0;
            rightCar.Y = 2;      
            leftCar.Direction = Direction.LEFT;
            leftCar.X = 3;
            leftCar.Y = 1;  
            upCar.Direction = Direction.UP;
            upCar.X = 2;
            upCar.Y = 3;         
            downCar.Direction = Direction.DOWN;
            downCar.X = 1;
            downCar.Y = 0;
            //Assert
            //Verify that every occupation is false
            for (int i = 0; i < tileArray.GetLength(1); i++)
            {
                for (int j = 0; j < tileArray.GetLength(0); j++)
                {
                    Assert.IsFalse(tileArray[i, j].Occupied);
                }
            }
            simpleGrid[downCar.X, downCar.Y].Occupied = true;
            simpleGrid[upCar.X, upCar.Y].Occupied = true;
            simpleGrid[leftCar.X, leftCar.Y].Occupied = true;
            simpleGrid[rightCar.X, rightCar.Y].Occupied = true;
            int tickCounter = 0;
            while (tickCounter < 9)
            {
                fs.Update();
                rightCar.Move(fs);
                leftCar.Move(fs);
                upCar.Move(fs);
                downCar.Move(fs);
                //Track vehicle movements
                if (tickCounter == 0)
                {
                    //These vehicles don't move - retest when they move
                    Assert.IsTrue(tileArray[1, 0].Occupied);
                    Assert.IsTrue(tileArray[2, 3].Occupied);
                    //These vehicles move
                    Assert.IsTrue(tileArray[2, 1].Occupied);
                    Assert.IsTrue(tileArray[1, 2].Occupied);
                    //And they remove their previous occupation
                    Assert.IsFalse(tileArray[0, 1].Occupied);
                    Assert.IsFalse(tileArray[3, 2].Occupied);
                }
                else if (tickCounter == 1)
                {
                    //These vehicles move
                    Assert.IsTrue(tileArray[2, 2].Occupied);
                    Assert.IsTrue(tileArray[1, 1].Occupied);
                    //And they remove their previous occupation
                    Assert.IsFalse(tileArray[2, 1].Occupied);
                    Assert.IsFalse(tileArray[1, 2].Occupied);
                }
                else if (tickCounter == 2)
                {
                    //These vehicles move
                    Assert.IsTrue(tileArray[0, 1].Occupied);
                    Assert.IsTrue(tileArray[3, 2].Occupied);
                    //And they remove their previous occupation
                    Assert.IsFalse(tileArray[2, 1].Occupied);
                    Assert.IsFalse(tileArray[1, 2].Occupied);
                }
                else if (tickCounter == 3)
                {
                    //These vehicules are off the grid
                    Assert.IsFalse(tileArray[0, 1].Occupied);
                    Assert.IsFalse(tileArray[3, 2].Occupied);
                }
                else if (tickCounter == 5)
                {
                    //These vehicles move
                    Assert.IsTrue(tileArray[2, 2].Occupied);
                    Assert.IsTrue(tileArray[1, 1].Occupied);
                }
                else if (tickCounter == 6)
                {
                    //These vehicles move
                    Assert.IsTrue(tileArray[1, 2].Occupied);
                    Assert.IsTrue(tileArray[2, 1].Occupied);
                    //They remove their previous accupation
                    Assert.IsFalse(tileArray[2, 2].Occupied);
                    Assert.IsFalse(tileArray[1, 1].Occupied);
                }
                else if (tickCounter == 7)
                {
                    //These vehicles move
                    Assert.IsTrue(tileArray[2, 0].Occupied);
                    Assert.IsTrue(tileArray[1, 3].Occupied);
                    //They remove their previous accupation
                    Assert.IsFalse(tileArray[1, 2].Occupied);
                    Assert.IsFalse(tileArray[2, 1].Occupied);
                }                else if (tickCounter == 8)
                {
                    //All cars have finished moving, everything is null
                    for (int i = 0; i < tileArray.GetLength(1); i++)
                    {
                        for (int j = 0; j < tileArray.GetLength(0); j++)
                        {
                            Assert.IsFalse(tileArray[i, j].Occupied);
                        }
                    }
                }
                tickCounter++;
            }
        }

        [TestMethod]
        public void TestMove2()
        {
            //Arrange
            FixedSignal fs = new FixedSignal(4, 2, 4, 2);
            Light ulight = new Light(fs, Direction.UP);
            Light dlight = new Light(fs, Direction.DOWN);
            Light llight = new Light(fs, Direction.LEFT);
            Light rlight = new Light(fs, Direction.RIGHT);
            Grass grass = new Grass();
            Road rup1 = new Road(Direction.UP);
            Road rup2 = new Road(Direction.UP);
            Road rup3 = new Road(Direction.UP);
            Road rup4 = new Road(Direction.UP);
            Road rdown1 = new Road(Direction.DOWN);
            Road rdown2 = new Road(Direction.DOWN);
            Road rdown3 = new Road(Direction.DOWN);
            Road rdown4 = new Road(Direction.DOWN);
            Road rleft1 = new Road(Direction.LEFT);
            Road rleft2 = new Road(Direction.LEFT);
            Road rleft3 = new Road(Direction.LEFT);
            Road rleft4 = new Road(Direction.LEFT);
            Road rright1 = new Road(Direction.RIGHT);
            Road rright2 = new Road(Direction.RIGHT);
            Road rright3 = new Road(Direction.RIGHT);
            Road rright4 = new Road(Direction.RIGHT);

            IntersectionTile it1 = new IntersectionTile();
            IntersectionTile it2 = new IntersectionTile();
            IntersectionTile it3 = new IntersectionTile();
            IntersectionTile it4 = new IntersectionTile();
            Tile[,] tileArray = { { grass, grass, rright1, rleft4, grass, grass},
                                  { grass, ulight, rright2, rleft3, llight, grass},
                                  { rup4, rup3, it1, it2, rup2, rup1},
                                  { rdown1, rdown2, it3, it4, rdown3, rdown4},
                                  { grass, rlight, rright3, rleft2, dlight, grass },
                                  { grass, grass, rright4, rleft1, grass, grass} };
            Grid simpleGrid = new Grid(tileArray);
            Car rightCar = new Car(simpleGrid);
            Car leftCar = new Car(simpleGrid);
            Car upCar = new Car(simpleGrid);
            Car downCar = new Car(simpleGrid);
            //Act 
            rightCar.Direction = Direction.RIGHT;
            rightCar.X = 0;
            rightCar.Y = 3;
            leftCar.Direction = Direction.LEFT;
            leftCar.X = 5;
            leftCar.Y = 2;
            upCar.Direction = Direction.UP;
            upCar.X = 3;
            upCar.Y = 5;
            downCar.Direction = Direction.DOWN;
            downCar.X = 2;
            downCar.Y = 0;
            //Act
            simpleGrid[downCar.X, downCar.Y].Occupied = true;
            simpleGrid[upCar.X, upCar.Y].Occupied = true;
            simpleGrid[leftCar.X, leftCar.Y].Occupied = true;
            simpleGrid[rightCar.X, rightCar.Y].Occupied = true;
            //Test if vehicles move once
            fs.Update();
            rightCar.Move(fs);
            leftCar.Move(fs);
            upCar.Move(fs);
            downCar.Move(fs);
            //Assert
            Assert.IsTrue(simpleGrid[1, 3].Occupied);
            Assert.IsTrue(simpleGrid[4, 2].Occupied);
            Assert.IsTrue(simpleGrid[3, 4].Occupied);
            Assert.IsTrue(simpleGrid[2, 1].Occupied);
        }

        private void printGrid(Grid g)
        {
            for (int i = 0; i < g.Size; i++)
            {
                for (int j = 0; j < g.Size; j++)
                {
                    Console.Write(g[j,i].Occupied + " ");
                }
                Console.WriteLine();
                
            }
            Console.WriteLine();
        }
    }
}